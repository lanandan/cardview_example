package com.example.cardview;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ListAdapter extends BaseAdapter{

	ArrayList<String> name,mail;
	ArrayList<Integer> image;
	Context cxt;
	int res;
	LayoutInflater inflater;
	Resources resor;
	View vi;
	
	ListAdapter(Context context, ArrayList<String> name, ArrayList<String> mail, ArrayList<Integer> image, int res){
		this.image = image;
		this.name = name;
		this.mail = mail;
		this.cxt = context;
		this.res = res;
		
				
	}
	
	class ViewHolder{
		TextView txt_nam,txt_mail;
		ImageView img;
		CardView cv;
		LinearLayout ll1;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		
		return name.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		//vi = new View(cxt);
		//vi = convertView;
        final ViewHolder holder;
        
        /*name = new ArrayList<String>();
		mail = new ArrayList<String>();
		image = new ArrayList<Integer>();*/
		
		if(convertView == null){
			
			convertView = LayoutInflater.from(cxt).inflate(R.layout.activity_row, parent, false); 
					
					//inflater.inflate(res, parent, false);
						
			holder = new ViewHolder();
			holder.txt_nam = (TextView)convertView.findViewById(R.id.person_name);
			holder.txt_mail = (TextView)convertView.findViewById(R.id.person_age);
			holder.img = (ImageView)convertView.findViewById(R.id.person_photo);
			holder.cv = (CardView)convertView.findViewById(R.id.cv1);
			//holder.ll1 = (LinearLayout)convertView.findViewById(R.id.ll_1);
			
			convertView.setTag( holder );
		}else {
            holder=(ViewHolder)convertView.getTag();
		}
		
		holder.txt_nam.setText(name.get(position));
		holder.txt_mail.setText(mail.get(position));
		holder.img.setImageResource(res);
		holder.cv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
	
		
		return convertView;
	}

}
