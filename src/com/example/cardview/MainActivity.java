package com.example.cardview;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	/*CardView cd1,cd2,cd3,cd4;
	TextView tv2,tv3,tv4;
	ImageView iv2,iv3,iv4;*/
	
	ListView lst_data;
	ListAdapter la;
	ArrayList<String> name,mail;
	ArrayList<Integer> image;
	int res = R.drawable.person;
	Toolbar tb;
	
	Context cxt;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		tb = (Toolbar)findViewById(R.id.toolbar);
		
		setSupportActionBar(tb);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tb.setTitle("Card View");
		
		lst_data = (ListView)findViewById(R.id.lst_crd); 
		name = new ArrayList<String>();
		mail = new ArrayList<String>();
		image = new ArrayList<Integer>();
		for(int i=0;i<7;i++){
			name.add("Name"+(i+1));
			mail.add("name"+(i+1)+"@example.com");
			image.add(res);
			Log.i("Check",""+name.toString()+mail.toString()+image.toString());
			}
		
		cxt = getApplicationContext();
		la = new ListAdapter(cxt, name, mail, image, res);
		 	 
		 lst_data.setAdapter(la);
		/* 
		 lst_data.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				lst_data.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,R.anim.zoom_in));
				Toast.makeText(getApplicationContext(), "Name = "+name.get(position) +" & "+"email-id = "+mail.get(position), Toast.LENGTH_SHORT).show();
			}

			
		});*/
      
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
